package com.example.miequipo.Register;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miequipo.R;

public class RegisterPlayer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_player);

        TextView txtBack = (TextView) findViewById(R.id.txtBack);

        txtBack.setText(Html.fromHtml("<u>volver</u>"));

        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button btnContinue = (Button) findViewById(R.id.btncontinue);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), RegisterPSW.class);
                startActivityForResult(myIntent, 0);
            }
        });

        Spinner footSpinner = (Spinner) findViewById(R.id.spinner_foot);
        Spinner positionSpinner = (Spinner) findViewById(R.id.spinner_position);
        Spinner levelSpinner = (Spinner) findViewById(R.id.spinner_level);


        // Create an ArrayAdapter using the string array and a default spinner

        ArrayAdapter<CharSequence> footAdapter;
        ArrayAdapter<CharSequence> positionAdapter;
        ArrayAdapter<CharSequence> levelAdapter;

        footAdapter = ArrayAdapter.createFromResource(this, R.array.Pie, R.layout.spinner_item);
        positionAdapter = ArrayAdapter.createFromResource(this, R.array.Position, R.layout.spinner_item);
        levelAdapter = ArrayAdapter.createFromResource(this, R.array.Level, R.layout.spinner_item);


        // Specify the layout to use when the list of choices appears
        footAdapter.setDropDownViewResource(R.layout.spinner_dropdrown);
        positionAdapter.setDropDownViewResource(R.layout.spinner_dropdrown);
        levelAdapter.setDropDownViewResource(R.layout.spinner_dropdrown);


        // Apply the adapter to the spinner
        footSpinner.setAdapter(footAdapter);
        positionSpinner.setAdapter(positionAdapter);
        levelSpinner.setAdapter(levelAdapter);

        footSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        positionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        levelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }



}