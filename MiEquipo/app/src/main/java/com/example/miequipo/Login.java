package com.example.miequipo;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miequipo.Register.Register;

public class Login extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        TextView txtIniciarSesion = (TextView) findViewById(R.id.txtSesion);

        txtIniciarSesion.setText(Html.fromHtml("Si ya tienes una cuenta, <u>inicia sesión</u>"));

        Button btnLogIn = (Button) findViewById(R.id.btnIniciarSesion);
        Button btnSignIn = (Button) findViewById(R.id.btnCrearCuenta);

        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), LoginSession.class);
                startActivityForResult(myIntent, 0);
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Register.class);
                startActivityForResult(myIntent, 0);
            }
        });
    }
}
