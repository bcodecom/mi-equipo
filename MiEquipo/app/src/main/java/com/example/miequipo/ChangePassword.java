package com.example.miequipo;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ChangePassword extends AppCompatActivity {

    ImageButton btnActual, btnNew, btnConfirm;
    EditText txtActual, txtNew, txConfirm;
    boolean showpassword1, showpassword2, showpassword3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);

        btnActual = (ImageButton) findViewById(R.id.btnActual);
        btnNew = (ImageButton) findViewById(R.id.btnNew);
        btnConfirm = (ImageButton) findViewById(R.id.btnConfirm);

        txtActual = (EditText) findViewById(R.id.txtActual);
        txtNew = (EditText) findViewById(R.id.txtNew);
        txConfirm = (EditText) findViewById(R.id.txtConfirm);

        btnActual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showpassword1 == true){
                    txtActual.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showpassword1 = false;
                }else{
                    txtActual.setTransformationMethod(null);
                    showpassword1 = true;
                }
            }
        });

        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showpassword2 == true){
                    txtNew.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showpassword2 = false;
                }else{
                    txtNew.setTransformationMethod(null);
                    showpassword2 = true;
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showpassword3 == true){
                    txConfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showpassword3 = false;
                }else{
                    txConfirm.setTransformationMethod(null);
                    showpassword3 = true;
                }
            }
        });

    }
}
