package com.example.miequipo;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miequipo.Fragments.Home;
import com.example.miequipo.Register.RegisterPSW;

public class LoginSession extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_sesion);
        TextView txtBack = (TextView) findViewById(R.id.txtBack);
        txtBack.setText(Html.fromHtml("<u>volver</u>"));

        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button btnContinue = (Button) findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Home.class);
                startActivityForResult(myIntent, 0);
            }
        });

        TextView txtForgot = (TextView) findViewById(R.id.txtForgot);
        txtForgot.setText(Html.fromHtml("<u>Se me olvidó la contraseña</u>"));
    }


}
