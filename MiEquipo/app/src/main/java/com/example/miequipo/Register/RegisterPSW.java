package com.example.miequipo.Register;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miequipo.ConfirmationAccount;
import com.example.miequipo.Fragments.Home;
import com.example.miequipo.R;
import com.example.miequipo.Tutorial;

public class RegisterPSW extends AppCompatActivity {

    boolean showPassword1 = false;
    boolean showPassword2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_psw);

        final RelativeLayout modal = (RelativeLayout) findViewById(R.id.modal);
        Button btnAccept = (Button) findViewById(R.id.btnAccept);
        Button btnContinue = (Button) findViewById(R.id.btncontinue);
        TextView txtBack = (TextView) findViewById(R.id.txtBack);

        txtBack.setText(Html.fromHtml("<u>volver</u>"));

        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), ConfirmationAccount.class);
                startActivityForResult(myIntent, 0);
            }
        });

        TextView politicas = (TextView) findViewById(R.id.txtPoliticas);
        politicas.setText(Html.fromHtml("Acepto políticas de <u><font color='#00FFBA'>términos y condiciones</font></u>"));
        politicas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modal.setVisibility(View.VISIBLE);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modal.setVisibility(View.GONE);
            }
        });

        ImageButton password = (ImageButton) findViewById(R.id.button1);
        ImageButton passwordconfirm = (ImageButton) findViewById(R.id.button2);

        final EditText txtpassword = (EditText) findViewById(R.id.txtpassword);
        final EditText txtconfirm = (EditText) findViewById(R.id.txtConfirm);

        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showPassword1 == true){
                    txtpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showPassword1 = false;
                }else{
                    txtpassword.setTransformationMethod(null);
                    showPassword1 = true;
                }
            }
        });

        passwordconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showPassword1 == true){
                    txtpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    showPassword2 = false;
                }else{
                    txtconfirm.setTransformationMethod(null);
                    showPassword2 = true;
                }
            }
        });
    }
}
