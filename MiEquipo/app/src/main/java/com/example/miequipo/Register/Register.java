package com.example.miequipo.Register;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.miequipo.LoginSession;
import com.example.miequipo.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Register extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        TextView txtBack = (TextView) findViewById(R.id.txtBack);

        txtBack.setText(Html.fromHtml("<u>volver</u>"));
        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button btnContinue = (Button) findViewById(R.id.btncontinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), RegisterPlayer.class);
                startActivityForResult(myIntent, 0);
            }
        });

        Spinner countriesSpinner = (Spinner) findViewById(R.id.static_spinner);
        Spinner citySpinner = (Spinner) findViewById(R.id.dynamic_spinner);


        // Create an ArrayAdapter using the string array and a default spinner

        ArrayAdapter<CharSequence> staticAdapter;
        ArrayAdapter<CharSequence> dynamicAdapter;

        dynamicAdapter = ArrayAdapter.createFromResource(this, R.array.City, R.layout.spinner_item);
        staticAdapter = ArrayAdapter.createFromResource(this, R.array.Countries, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears

        staticAdapter.setDropDownViewResource(R.layout.spinner_dropdrown);
        dynamicAdapter.setDropDownViewResource(R.layout.spinner_dropdrown);

        // Apply the adapter to the spinner
        countriesSpinner.setAdapter(staticAdapter);
        citySpinner.setAdapter(dynamicAdapter);

        countriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


    }
}
