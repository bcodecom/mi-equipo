package com.example.miequipo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miequipo.Fragments.Home;
import com.example.miequipo.Register.RegisterPSW;

import java.text.MessageFormat;

public class Tutorial extends AppCompatActivity {

    CheckBox ch1,ch2,ch3;
    int position = 1;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    ImageView imgTutorial;
    TextView title;
    TextView description;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);

        TextView saltar = (TextView) findViewById(R.id.txtSaltar);
        saltar.setText(Html.fromHtml("<u>Salta, ya sé como funciona</u>"));

        saltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), Home.class);
                startActivityForResult(myIntent, 0);
            }
        });

        TextView txtBack = (TextView) findViewById(R.id.txtBack);
        txtBack.setText(Html.fromHtml("<u>volver</u>"));

        title = (TextView) findViewById(R.id.titleTutorial);
        description = (TextView) findViewById(R.id.description);

        ch1 = (CheckBox) findViewById(R.id.check1);
        ch2 = (CheckBox) findViewById(R.id.check2);
        ch3 = (CheckBox) findViewById(R.id.check3);
        RelativeLayout tutorials = (RelativeLayout) findViewById(R.id.tutorialTexts);
        imgTutorial = (ImageView) findViewById(R.id.img_tutorial);

        tutorials.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x2 - x1;

                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            // izquiera a derecha
                            if (x2 > x1) {
                                if (position != 1 ){
                                    position--;
                                    setDescriptionTitle(position);
                                }else{
                                    position = 3;
                                    setDescriptionTitle(position);
                                }
                            }
                            // derecha a izquierda
                            else {
                                if (position != 3 ){
                                    position++;
                                    setDescriptionTitle(position);
                                }else{
                                    position = 1;
                                    setDescriptionTitle(position);
                                }
                            }

                        }
                        break;
                }
                return true;
            }
        });


    }

    void setDescriptionTitle(int position){

        switch (position){
            case 1:
                title.setVisibility(View.INVISIBLE);
                imgTutorial.setVisibility(View.VISIBLE);
                title.setText(R.string.title1);
                description.setText(R.string.description1);
                ch1.setChecked(true);
                ch2.setChecked(false);
                ch3.setChecked(false);
                break;

            case 2:
                title.setVisibility(View.VISIBLE);
                imgTutorial.setVisibility(View.INVISIBLE);
                title.setText(R.string.title2);
                description.setText(R.string.description2);
                ch1.setChecked(false);
                ch2.setChecked(true);
                ch3.setChecked(false);
                break;
            case 3:
                title.setVisibility(View.VISIBLE);
                imgTutorial.setVisibility(View.INVISIBLE);
                title.setText(R.string.title3);
                description.setText(R.string.description3);
                ch1.setChecked(false);
                ch2.setChecked(false);
                ch3.setChecked(true);
                break;
        }

    }
}
